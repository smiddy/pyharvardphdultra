import serial
import logging
from datetime import datetime
from time import sleep

# Set version
__version__ = '0.1.0-beta1'

# create logger
logger = logging.getLogger(__name__)


class PHDUltraError(Exception):
    """
    PHDUltra exception handle
    """

    def __init__(self, command, output):
        self.logger = logging.getLogger(__name__)
        # Command error
        if output[1] == 'Command error: ':
            self.errStr = output[1] + output[2] + '. Was "{}"'.format(command)
        # Argument error
        elif 'Argument error' in output[1]:
            self.errStr = output[1] + output[2]
        elif output[-1] == '*':
            self.errStr = 'Pump is stalled.'
        self.logger.error(self.errStr)
        return

    def __str__(self):
        return self.errStr

    '''Python SDK for the Harvard PHD Ultra

    Here comes the description. This is followed by the definition of the input parameters and usually a nice example.

    :param comPort: address of serial port
    :type comPort: string

    Example::
        import phdultra
        import logging
        
        port = 'COM6'
        
        # Create logger
        logging.basicConfig(level=logging.DEBUG, filename='example.log',
                            format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        
        with phdultra.Device(port) as pump:
            print(pump.sendrcv('ver'))
            print(pump.sendrcv('dim 90%'))
            print(pump.sendrcv('version'))
            print(pump.sendrcv('time'))
            pump.sendrcv('diameter 27')
            pump.sendrcv('svolume 60 ml')
        pass

    '''


class Device(object):
    def __init__(self, comport, rate=115200):
        """ Device class for Harvard PHD Ultra syringe pump

        :param comport: address of serial port
        :param rate: baudrate
        """
        self.__version__ = __version__
        self.logger = logging.getLogger(__name__)
        self.comLink = serial.Serial(comport, baudrate=rate,
                                     stopbits=serial.STOPBITS_ONE,
                                     bytesize=serial.EIGHTBITS,
                                     timeout=0.5,
                                     parity=serial.PARITY_NONE)
        self._noqueries = ['irun', 'rrun', 'stop', 'stp', 'wrun']
        if not self.comLink.isOpen():
            self.comLink.open()
        # Empty the read cache
        _ = self.comLink.read(self.comLink.in_waiting)
        self.sendrcv('cmd ultra')
        self.sendrcv('STP')
        self.sendrcv('time ' + datetime.now().strftime('%d/%m/%yy %H:%M:%S'))
        self.logger.debug("Pump successful initialized")

    def sendrcv(self, command):
        """Takes a command and send it

        The string is converted to a compatible byte and send
        to the delay generator. The answer is analysed and in
        case a PHDUltra is raised.

        :param command: Command string
        :type: string
        :returns: answer
        :rtype: string
        """
        hasanswer = True
        # Assume a whitespace for non query commands
        if (' ' in command or command.lower() in self._noqueries) and '?' not in command:
            hasanswer = False
        sendbyte = bytearray(command, 'ASCII') + b'\r'
        if not self.comLink.isOpen():
            self.comLink.open()
        self.comLink.write(sendbyte)
        sleep(0.12)
        # Workaround: Sleep longer if we call for syrm, otherwise response is cropped
        if command.lower() in ['syrm ?', 'irun', 'wrun', 'rrun', 'run', 'wvolume', 'ivolume']:
            sleep(0.08)
        while not (self.comLink.out_waiting == 0):
            sleep(0.1)
        response = self.comLink.read(self.comLink.in_waiting)
        if not response:
            self.logger.error('No response from PHD Ultra received')
            raise ValueError('No response from PHD Ultra received')
        output = response.decode('ASCII')
        output = output.splitlines()
        # here comes the error handling
        if output[1] == 'Command error: ':
            raise PHDUltraError(command, output)
        elif 'Argument error' in output[1]:
            raise PHDUltraError(command, output)
        elif output[-1] == '*':
            raise PHDUltraError(command, output)
        self.logger.debug('Command "{}" successful executed.'.format(command))
        if hasanswer:
            if len(output) == 3:
                return output[1]
            else:
                return output[1:-1]
        else:
            return

    def resettoinfuse(self):
        tvolume = self.sendrcv('tvolume')
        irate = self.sendrcv('irate')
        self.sendrcv('tvolume 0.1 ul')
        self.sendrcv('civolume')
        self.sendrcv('irate 0.1 ml/min')
        self.sendrcv('irun')
        self.sendrcv('stp')
        self.sendrcv('irate ' + irate)
        self.sendrcv('tvolume ' + tvolume)
        self.sendrcv('civolume')

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if self.comLink.isOpen():
            self.sendrcv('STP')
            self.comLink.close()

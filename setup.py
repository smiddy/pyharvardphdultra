from setuptools import setup
from phdultra import __version__


setup(
    name="phdultra",
    description="Python SDK for the Harvard PHD Ultra Syringe Pump",
    version=__version__,
    platforms=["any"],
    author="Markus J Schmidt",
    author_email='schmidt@ifd.mavt.ethz.ch',
    license="GNU GPLv3+",
    url="https://gitlab.ethz.ch/ifd-lab/device-driver/pyharvardphdultra",
    py_modules=["phdultra"],
    install_requires=['pyserial>=3.4'],
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering',
        'License :: OSI Approved :: GNU General Public License v3 '
        'or later (GPLv3+)',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7'
    ],
    zip_safe=False
)

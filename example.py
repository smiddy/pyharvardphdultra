import phdultra
import logging

port = 'COM6'

# Create logger
logging.basicConfig(level=logging.DEBUG, filename='example.log',
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

with phdultra.Device(port) as pump:
    print(pump.sendrcv('ver'))
    pump.resettoinfuse()
    print(pump.sendrcv('dim 90%'))
    print(pump.sendrcv('version'))
    print(pump.sendrcv('time'))
    pump.sendrcv('diameter 27')
    pump.sendrcv('svolume 60 ml')
pass

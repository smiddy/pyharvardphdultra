# pyHarvardPHDUltra

Python API for the Harvard PHD Ultra

Tested with
* Win10 64bit
* Python 3.7.3 (Anaconda)
* pyserial 3.4

## Installation
Installation from project folder in current Python environment with

```python
python setup.py install
```

## Classes
The class phdultra.Device is the main class for interaction. Commands can be executed
with the `sendrcv` method. If command is a query, the function returns the answer as a string or float.

The class `phdultra.PHDUltraError` issues errors.

A context manager is availabe for safe operation. Please check `example.py` for an example.
